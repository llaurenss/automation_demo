var YoutubeHome = function() {
    browser.ignoreSynchronization = true;
    browser.get('https://samirhodzic.github.io/ngx-youtube-player/');
};

YoutubeHome.prototype = Object.create({}, {
    input: { get: function() { return element(by.id('query')); } },
    button: { get: function() { return element(by.xpath('/html/body/app/main-list/div[1]/div/header/div/nav/videos-search/form/button')); } },

    searchQuery: {
        value: function(query) {
            this.input.sendKeys(query);
            this.button.click();
        }
    },

    isInputBoxVisible: {
        value: function() { return this.input.isDisplayed(); }
    },

    isSearchButtonVisible: {
        value: function() { return this.button.isDisplayed(); }
    },
});

module.exports = YoutubeHome;