describe('angular todo list', function() {
    it("the todo should be empty!", function() {
        browser.get('http://todomvc.com/examples/angularjs/#/');
        var todoList = element.all(by.repeater('todo in todos'));
        expect(todoList.count()).toEqual(0);
    });

    it('should add a todo', function() {
        element(by.id('new-todo')).sendKeys('Write our first protractor test!');
        browser.actions().sendKeys(protractor.Key.ENTER).perform();

        var todoList = element.all(by.repeater('todo in todos'));
        expect(todoList.count()).toEqual(1);
        expect(todoList.get(0).getText()).toEqual('Write our first protractor test!');

        /*element.all(by.repeater('todo in todos').row(0)).getText().then(function(text) {
            console.log(text);
        });*/
    });

    it('The task if not completed should not be checked and the "clear completed" should not be displayed!', function() {
        var todoList = element.all(by.repeater('todo in todos'));
        var checkbox = todoList.get(0).element(by.model('todo.completed'));

        expect(checkbox.isSelected()).toBeFalsy();
        expect(element(by.id('clear-completed')).isDisplayed()).toBeFalsy();
    });

    it('If we mark the task completed the checkbox should be on and the label "Clear completed" should be displayed', function() {
        var todoList = element.all(by.repeater('todo in todos'));
        var checbox = todoList.get(0).element(by.model('todo.completed'));
        checbox.click();
        expect(checbox.isSelected()).toBeTruthy();
        expect(element(by.id('clear-completed')).isDisplayed()).toBeTruthy();
    });

    it('When we are done with our tasks we will clear the completed ones!', function() {
        var todoList = element.all(by.repeater('todo in todos'));

        element(by.id('clear-completed')).click();
        expect(todoList.count()).toEqual(0);
    });
});