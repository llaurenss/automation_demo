var YoutubeHome = require('../page_objects/YoutubePage.js');

describe('youtube perform a search', function() {
    var youtubeHome;

    beforeEach(function() {
        youtubeHome = new YoutubeHome();
    });

    it('Check if the inputbox and searchbox are displayed!', function() {
        //var button = element(by.id('.//*[.="Locaties toevoegen"]'))
        expect(youtubeHome.isInputBoxVisible()).toBeTruthy();
        expect(youtubeHome.isSearchButtonVisible()).toBeTruthy();
    });
});