exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    //specs: ['todo-spec.js', 'youtube-spec.js'],
    suites: {
        todo: 'todo/*-spec.js',
        youtube: ['youtube/*-spec.js']
    },

    jasmineNodeOpts: {
        showColors: true, // Use colors in the command line report.
    }
};